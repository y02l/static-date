import datetime
import json
'''
card-data.json
{
 name:
  {
   ruby: str
   is02: bool
   imgs: set()
  }
}
'''


if __name__ == '__main__':
    with open('data.json', 'r') as f:
        data = json.load(f)
    with open('img-data.json', 'r') as f:
        img_data = json.load(f)

    card_data = {
        name: dict(is02=False, imgs_path=list(set(imgs_url)))
        for name, imgs_url in img_data.items()
    }

    for pack in data:
        date = datetime.datetime.strptime(pack['date'], '%Y-%m-%d')
        for card in pack['cards']:
            card_data[card['name']]['ruby'] = card['ruby']
            if datetime.datetime(2002, 5, 1) > date:
                card_data[card['name']]['is02'] = True

    with open('card-data.json', 'w') as f:
        json.dump(card_data, f, ensure_ascii=False, indent=2)

    # add full ruby
    with open('card-data.json', 'r') as f:
        data = json.load(f)
