import json
import os
import re
import requests
import time
import urllib

from tqdm import tqdm
from bs4 import BeautifulSoup


def absolute_url(relative_url):
    """
    db.yugioh_card.com内での相対パスを絶対パスに変換する
    """
    if 'https://' in relative_url:
        return relative_url

    url_base = 'https://www.db.yugioh-card.com'
    to_ja = ('&' if '?' in relative_url else '?') + 'request_locale=ja'
    return url_base + relative_url + to_ja


def shrink_html(html):
    shrinked_html = re.sub(r'[\r|\t]', '', html)
    return shrinked_html


def get_html(url, js=False):
    """
    urlからhtmlを返す
    """
    if js:
        return get_jsed_html(url)
    else:
        return get_normal_html(url)


def get_normal_html(url):
    """
    urlからhtmlを返す
    """
    time.sleep(1)
    html = requests.get(absolute_url(url)).text

    # スクレイピングデータに使われない文字を消す
    return shrink_html(html)


def get_jsed_html(url):
    """
    urlからjavascriptを実行したhtmlを返す
    """
    host = 'localhost:30080'
    url = 'http://' + host + '?q=' + urllib.parse.quote(absolute_url(url),
                                                        safe='')
    html = requests.get(url).text
    return shrink_html(html)


def get_cardimg(img_url, card_url, fp=os.devnull):
    img = requests.get(absolute_url(img_url),
                       headers={
                           'referer': absolute_url(card_url)
                       }).content
    with open(fp, 'wb') as f:
        f.write(img)
    return img


'''
data

[
 dict(
  url: str,
  name: str,
  ruby: str,
  cards: [dict(url: str, name: str, imgs_url: [url])]
 )
]
'''


def html_to_soup(html):
    return BeautifulSoup(html, "html.parser")


def get_packs_url(packs_html):
    """
    パックのurlのリストを返す
    """
    soup = html_to_soup(packs_html)

    return [
        str(pack.find('input')['value'])
        for pack in soup.find_all('div', class_='pack pack_ja')
    ]


def get_pack_data(pack_html):
    """
    パックの情報とカードのurlのリストを返す
    """
    soup = html_to_soup(pack_html)
    broad_title = soup.find('header', id='broad_title')

    name = broad_title.find('h1').text

    date_text = broad_title.find('p', id='previewed').text
    match = re.search(r'[0-9]+年[0-9]+月[0-9]+', date_text)
    date = re.sub(r'[年月]', '-', match.group(0)) if match else None

    cards_url = [
        str(card.find('input')['value'])
        for card in soup.find('ul', class_='box_list').find_all('li')
    ]

    return dict(name=str(name), date=str(date)), cards_url


def get_card_data(card_html):
    """
    カードの情報を返す
    """
    soup = html_to_soup(card_html)

    broad_title = soup.find('header', id='broad_title')
    details = soup.find('table', id='details')

    name = str(broad_title.find('h1').contents[2])
    name = name.replace('\n', '')
    ruby = broad_title.find('span', class_='ruby').text
    ruby = str(ruby)

    thumbnails = soup.find_all('div', id='thumbnail')
    imgs_url = [t.find('img')['src'] for t in thumbnails]

    tdname_key_dict = dict(属性='attribute',
                           レベル='level',
                           種族='monster_type',
                           その他項目='card_type',
                           効果='card_type',
                           攻撃力='atk_',
                           守備力='def_',
                           カードテキスト='card_text')
    for td in details.find_all('td'):
        tdname = td.find('b').text
        key = tdname_key_dict.get(tdname, None)
        value = None
        if key in ['attribute', 'level', 'atk_', 'def_']:
            value = td.find('span', class_='item_box_value').text
        elif key in ['monster_type', 'card_text']:
            value = td.find('div').contents[2]
        elif key in ['card_type']:
            value = td.find('div').contents[2].replace('／', ',')
        else:
            continue
        value = re.sub(r'[\n|\t|\s]', '', str(value))

    return dict(name=name, ruby=ruby, imgs_url=imgs_url)


if __name__ == '__main__':
    if not os.path.exists('data.json'):
        with open('data.json', 'w') as f:
            json.dump([], f, ensure_ascii=False, indent=2)

    with open('data.json', 'r') as f:
        data = json.load(f)

    packs_url = get_packs_url(
        get_html('/yugiohdb/card_list.action', js=True))

    exists_packs_url = [d['url'] for d in data]
    new_packs_url = [url for url in packs_url if url not in exists_packs_url]

    for pack_url in tqdm(new_packs_url):
        pack, cards_url = get_pack_data(get_html(pack_url, js=True))

        cards = []
        for card_url in tqdm(cards_url):
            card = get_card_data(get_html(card_url, js=True))
            card['url'] = card_url
            cards.append(card)

        with open('data.json', 'r') as f:
            data = json.load(f)
        pack['url'] = pack_url
        pack['cards'] = cards
        data.append(pack)
        with open('data.json', 'w') as f:
            json.dump(data, f, ensure_ascii=False, indent=2)
