import os
import re
import requests
import time
import urllib

import filetype


def absolute_url(relative_url):
    """
    db.yugioh_card.com内での相対パスを絶対パスに変換する
    """
    if 'https://' in relative_url:
        return relative_url

    url_base = 'https://www.db.yugioh-card.com'
    to_ja = ('&' if '?' in relative_url else '?') + 'request_locale=ja'
    return url_base + relative_url + to_ja


def shrink_html(html):
    shrinked_html = re.sub(r'[\r|\t]', '', html)
    return shrinked_html


def get_html(url, js=False):
    """
    urlからhtmlを返す
    """
    if js:
        return get_jsed_html(url)
    else:
        return get_normal_html(url)


def get_normal_html(url):
    """
    urlからhtmlを返す
    """
    time.sleep(1)
    html = requests.get(absolute_url(url)).text

    # スクレイピングデータに使われない文字を消す
    return shrink_html(html)


def get_jsed_html(url):
    """
    urlからjavascriptを実行したhtmlを返す
    """
    host = 'localhost:30080'
    url = 'http://' + host + '?q=' + urllib.parse.quote(absolute_url(url),
                                                        safe='')
    html = requests.get(url).text
    return shrink_html(html)


def get_cardimg(img_url, card_url, fp=os.devnull):
    time.sleep(1)
    resp = requests.get(absolute_url(img_url),
                        headers={'referer': absolute_url(card_url)})
    if resp.status_code != 200:
        return None

    img = resp.content
    kind = filetype.guess(img)
    if kind is None:
        return None
    if 'image' not in kind.mime:
        return None

    with open(fp, 'wb') as f:
        f.write(img)
    return img
