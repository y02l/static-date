import json
import os
import sys

from tqdm import tqdm

import util
'''
img-data
{
 name1: [],
 name2: [],
}
'''

DEST_DIR = './imgs'

if __name__ == '__main__':
    with open('data.json', 'r') as f:
        data = json.load(f)

    if not os.path.exists('img-data.json'):
        with open('img-data.json', 'w') as f:
            json.dump(dict(), f, ensure_ascii=False, indent=2)
    with open('img-data.json', 'r') as f:
        img_data = json.load(f)

    cards = []
    for pack in data:
        cards.extend(pack['cards'])
    exists_cards_name = img_data.keys()
    new_cards = [
        card for card in cards if card['name'] not in exists_cards_name
    ]

    for card in tqdm(new_cards):
        img_url = card['imgs_url'][0].replace('&ciid=1', '')
        for index in range(1, sys.maxsize):
            path = card['name'] + '__' + str(index) + '.png'
            img = util.get_cardimg(img_url + '&ciid=' + str(index),
                                   card['url'], DEST_DIR + '/' + path)
            if img is None:
                break

            with open('img-data.json', 'r') as f:
                img_data = json.load(f)
            paths = img_data.get(card['name'], [])
            paths.append(path)
            img_data[card['name']] = paths
            with open('img-data.json', 'w') as f:
                json.dump(img_data, f, ensure_ascii=False, indent=2)
